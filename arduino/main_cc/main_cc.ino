#include <SPI.h>
#include <Ethernet.h>

#define BOMBA 4
#define SENSOR_PISCINA 2
#define SENSOR_COLETOR 4
#define MEDIA_LEITURAS 200
#define PORTA 8080

const float REFERENCIA_MILIVOLTS = 1.1/1024;
const IPAddress WEBSERVICE(10,24,7,30);

byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};
float temperaturaDesejada;
EthernetClient client;
boolean status = false;

void setup() {  
  pinMode(BOMBA, OUTPUT);
  digitalWrite(BOMBA, HIGH);
  
  Serial.begin(9600);
  analogReference(INTERNAL); // Referencia de 1.1V UNO (1.1/1024=0,913mv) 0,913mv
  
  Serial.println("Inciando ...");
}

void loop() {
  boolean erro = false;
   Serial.print("temperatura Piscina:");
  float temperaturaPiscina = lerTemperaturaSensor(SENSOR_PISCINA);
  
  Serial.print("temperatura Coletor:");
  float temperaturaColetor = lerTemperaturaSensor(SENSOR_COLETOR);
  
  
  int tentativas = 1;
  while (tentativas < 10){
    Serial.print("Tentativa de conexao numero: ");
    Serial.println(tentativas);
    
    temperaturaDesejada = buscarTemperaturaDesejada(erro);

    Serial.print("temperatura encontrada:");
    Serial.println(temperaturaDesejada);
    if (temperaturaDesejada != NULL) {
       tentativas = 10;
       Serial.println("leitura com sucesso");
    }   
    tentativas++;
  }
  Serial.print("temperatura desejada:");
  Serial.println(temperaturaDesejada);

  erro = falhaLeituraTemperatura(temperaturaPiscina, erro);
  erro = falhaLeituraTemperatura(temperaturaColetor, erro);
  erro = falhaLeituraTemperatura(temperaturaDesejada, erro);
    
  if (erro) {
    Serial.println("Desligando bomba, Erro");
    bombaOFF(BOMBA);  
  } else {
    if (status == false) {
      Serial.println("Desligando bomba, webservice enviou comando OFF");
      bombaOFF(BOMBA);
    } else {
      if (possoLigarBomba(temperaturaDesejada, temperaturaPiscina, temperaturaColetor)) {
        Serial.println("Ligando bomba, coletor atingiu temperatura para aquecer");
        bombaON(BOMBA);
      } else {
          Serial.println("Desligando bomba, Coletor esta mais frio que a piscina");        
          bombaOFF(BOMBA);
      }
    }
  }
  delay(6000);
}

float lerTemperaturaSensor(int sensor) {
  float leitura = 0;;

  for (int i=0; i < MEDIA_LEITURAS;i++) {
    leitura += analogRead(sensor);
    delay(5);
  }
  
  float media = leitura / MEDIA_LEITURAS;
  float temperatura = (media * REFERENCIA_MILIVOLTS) * 100;
  Serial.println(temperatura);
  return temperatura;
}  
  

float buscarTemperaturaDesejada(boolean erro) {
 
  Serial.println("Conectando na Rede");
  
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Falha ao adquirir o endereco IP via DHCP");
    erro = true;
    return 0;
  }
  
  Serial.print("My IP address: ");
  for (byte thisByte = 0; thisByte < 4; thisByte++) {
    Serial.print(Ethernet.localIP()[thisByte], DEC);
    Serial.print("."); 
  }
  Serial.println();
  
  delay(2000);

  EthernetClient client;
  Serial.println("Tentando conexao com o WebService.");
  
  if (client.connect(WEBSERVICE, PORTA)) {
      Serial.println("conectado.");
      Serial.println("Autenticando");
        
       client.println("GET /operacao HTTP/1.1");
       client.println("User-Agent: curl/7.37.1");
       client.println("Host: 10.1.1.25:8000");
       client.println("Content-Type: application/json");
       client.println("Authorization: Basic YXJkdWlubzpvaTJoM29pNDV1Z0YyNGpXcjM0NTQ="); // Base64 Encode "usuario:password"

       client.println();
       
       delay(500);
       String buffer= "";
       
       Serial.println("Processando a resposta");
       Serial.println("");
       while(client.available()) {
         char c = client.read();
         
         if (c != '\n') {
           buffer += c;
         }
         
       }
       Serial.println("Imprimindo o BUFFER.");
       Serial.println(buffer);
       Serial.println("Resposta recebida.");
       
       int inicio = buffer.indexOf("\{");
       int fim = buffer.lastIndexOf("\}") + 1;
       Serial.println(inicio);
       Serial.println(fim);
       String json (buffer.substring(inicio, fim)); 
       Serial.println();
       Serial.print("json:");
       Serial.println(json);
       
       int posicaoTemperatura = json.indexOf(":")+1;
       String temperatura = json.substring(posicaoTemperatura, json.indexOf(","));
       Serial.print("temperatura:");
       Serial.println(temperatura);
        
       int posicaoInicialStatus = json.indexOf(":", posicaoTemperatura)+1;
       Serial.println(posicaoInicialStatus);
       String valueStatus = json.substring(posicaoInicialStatus);
       Serial.print("tagStatus:");
       Serial.println(valueStatus);
       if (valueStatus == "true}") {
          status = true;
       } else {
        status = false;
       }   
       Serial.println(status);
       
       
       client.stop();
       return temperatura.toInt();
    }
}


boolean possoLigarBomba(float temperaturaDesejada, float temperaturaPiscina, float temperaturaColetor) {
  if (temperaturaPiscina < temperaturaDesejada) {
     if (temperaturaColetor > temperaturaPiscina) {
       return true;
     }
  }
  
  if (temperaturaPiscina >= temperaturaDesejada) {
     return false;
   }
   
   if (temperaturaColetor <= temperaturaPiscina) {
     return false;
   }
   return false;
}

boolean falhaLeituraTemperatura(float temperatura, boolean erro) {
  if (erro) return erro;
  
  if ((temperatura < 0) || (temperatura > 150)) {
    return true;
  }
  return false;
}

void bombaON(int bomba) {
 Serial.println("Ligando a Bomba"); 
 digitalWrite(bomba, LOW); 
}

void bombaOFF(int bomba) {
 Serial.println("Desligando a Bomba"); 
 digitalWrite(bomba, HIGH); 
}
