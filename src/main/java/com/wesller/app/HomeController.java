package com.wesller.app;

import com.wesller.app.domain.Operacao;
import com.wesller.app.util.Arquivo;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {

    private static Logger logger = Logger.getLogger(HomeController.class);

    @RequestMapping(value = "/")
    public String home(Model model) {
        Arquivo arquivo = Arquivo.of();
        Operacao operacao = arquivo.load();
        logger.debug("operacao:" + operacao);
        if (operacao == null) {
            operacao = Operacao.of(new Integer(15), true);
        }
        model.addAttribute("operacao", operacao);
        return "index";
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String save(@ModelAttribute Operacao operacao, Model model) {
        Arquivo arquivo = Arquivo.of();
        arquivo.save(operacao);
        model.addAttribute("temperatura", operacao.getTemperatura());
        logger.debug("operacao:" + operacao);
        return "index";
    }


}