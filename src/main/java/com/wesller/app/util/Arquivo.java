package com.wesller.app.util;


import com.wesller.app.domain.Operacao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class Arquivo implements Serializable {

    private static final String DEFAULT_EX = ".dat";

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public static Arquivo of() {
        return new Arquivo();
    }

    public void save(Operacao operacao) {
        try {
            FileOutputStream arquivo = new FileOutputStream(buildFileNamePath("hotpool"));
            ObjectOutputStream objStream = new ObjectOutputStream(arquivo);
            objStream.writeObject(operacao);
            objStream.flush();
            objStream.close();
            arquivo.flush();
            arquivo.close();
        } catch (Exception e) {
            logger.error("Erro ao salvar o arquivo", e);
        }
    }

    public Operacao load() {
        try {
            FileInputStream inputStream = new FileInputStream(buildFileNamePath("hotpool"));
            ObjectInputStream stream = new ObjectInputStream(inputStream);
            Object object = stream.readObject();
            stream.close();
            inputStream.close();
            return (Operacao) object;
        } catch (Exception e) {
            logger.error("Erro ao ler arquivo", e);
        }
        return null;
    }


    private void createFolder(String folder) {
        File file = new File(folder);
        if (!file.exists()) {
            file.mkdir();
        }
    }

    private String getDefaultFolder() {
        final String DEFAULT_FOLDER = "/hotpool/";
        String folder = String.format("%s%s", System.getProperty("user.home"), DEFAULT_FOLDER);
        logger.debug(String.format("Default Folder: %s", folder));
        createFolder(folder);
        return folder;
    }

    private String buildFileNamePath(String fileName) {
        String folder = getDefaultFolder();
        String fileNameWithPath = String.format("%s%s%s", folder, fileName, DEFAULT_EX);
        logger.debug(String.format("File Name :%s", fileNameWithPath));
        return fileNameWithPath;
    }

}
