package com.wesller.app.domain;


import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import java.io.Serializable;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkNotNull;

public class Operacao implements Serializable {

    private Integer temperatura;

    private boolean status;

    public Operacao() {
    }

    private Operacao(Integer temperatura, boolean status) {
        this.temperatura = temperatura;
        this.status = status;
    }

    public static Operacao of(Integer temperatura, boolean status) {
        checkNotNull(temperatura);

        return new Operacao(temperatura, status);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Operacao) {
            Operacao other = (Operacao) obj;
            return equal(this.temperatura, other.temperatura) &&
                    equal(this.status, other.status);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(temperatura, status);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("temperatura", temperatura)
                .add("status", status)
                .toString();
    }

    public Integer getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(Integer temperatura) {
        this.temperatura = temperatura;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isStatus() {
        return status;
    }
}
