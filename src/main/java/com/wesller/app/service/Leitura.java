package com.wesller.app.service;

import com.wesller.app.domain.Operacao;
import com.wesller.app.util.Arquivo;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;

@RestController
public class Leitura implements Serializable {

    @RequestMapping(method = RequestMethod.GET, value = "/operacao")
    public Operacao getOperacao() {
        Arquivo arquivo = Arquivo.of();
        return arquivo.load();
    }
}
